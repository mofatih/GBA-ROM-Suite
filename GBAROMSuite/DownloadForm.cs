﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.IO.Compression;
using MyGBALibrary.Properties;

namespace MyGBALibrary
{
    public partial class DownloadForm : Form
    {
        string curDllink, curFile, hackid, threadlink;
        Dictionary<string, string> versions = new Dictionary<string, string>();
        Main mainform;
        public DownloadForm(int hackid, Main mainform)
        {
            InitializeComponent();
            this.mainform = mainform;
            this.hackid = hackid.ToString();
            progressBar1.Visible = false;
            label5.Visible = false;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    var json = System.Net.WebUtility.HtmlDecode(wc.DownloadString("https://fecentral.org/api/getHack.php"));
                    JArray hacks = JArray.Parse(json);
                    json = wc.DownloadString("https://fecentral.org/api/getScreenshots.php");
                    JArray screenshots = JArray.Parse(json);
                    json = wc.DownloadString("https://fecentral.org/api/getVersions.php");
                    JArray versionsarray = JArray.Parse(json);
                    JObject currhack = hacks.Children<JObject>().FirstOrDefault(o => o["id"] != null && o["id"].ToString() == this.hackid);
                    label1.Text = currhack.Value<string>("name");
                    label2.Text = strings.By + currhack.Value<string>("author");
                    label4.Text = currhack.Value<string>("engine");
                    textBox1.Text = currhack.Value<string>("description");
                    this.threadlink = currhack.Value<string>("thread");
                    int thumbnail = currhack.Value<int>("thumbnail");
                    if (thumbnail != 0)
                    {
                        JObject screenshot = screenshots.Children<JObject>().FirstOrDefault(o => o["id"] != null && o["id"].ToString() == thumbnail.ToString());
                        pictureBox1.ImageLocation = "https://fecentral.org/uploads/screenshots/" + currhack.Value<string>("name").Replace(" ", "%20") + "/" + screenshot.Value<string>("file").Replace(" ", "%20");
                    }
                    else
                    {
                        JObject screenshot = screenshots.Children<JObject>().FirstOrDefault(o => o["hackid"] != null && o["hackid"].ToString() == this.hackid);
                        if(screenshot != null && screenshot.Value<string>("file").Length > 0) pictureBox1.ImageLocation = "https://fecentral.org/uploads/screenshots/" + currhack.Value<string>("name").Replace(" ", "%20") + "/" + screenshot.Value<string>("file").Replace(" ", "%20");
                        else pictureBox1.Image = Properties.Resources.No_Screenshots;
                    }
                    var currversions = versionsarray.Children<JObject>().Where(o => o["hackid"] != null && o["hackid"].ToString() == this.hackid).OrderBy(v => v.Value<int>("date")).Reverse();
                    foreach(JObject v in currversions)
                    {
                        versions[v.Value<string>("versionno")] = v.Value<string>("file");
                        comboBox1.Items.Add(v.Value<string>("versionno"));
                    }
                    comboBox1.SelectedIndex = 0;
                    if (versions.Count > 0)
                    {
                        curFile = versions[(string)comboBox1.Items[0]];
                        curDllink = "http://fecentral.org/uploads/games/" + curFile.Replace(" ", "%20");
                    }
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            curFile = versions[(string)comboBox1.Items[comboBox1.SelectedIndex]];
            curDllink = "http://fecentral.org/uploads/games/" + curFile.Replace(" ", "%20");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(threadlink);
            }
            catch(Exception ee)
            {
                MessageBox.Show(strings.GoToThreadError, "My GBA Library");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.fecentral.org/?page=hackinfo&hackid=" + hackid);
            }
            catch (Exception ee)
            {
                MessageBox.Show(strings.GoToPageError, "My GBA Library");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(curFile != null && curDllink != null)
            {
                Directory.CreateDirectory("Downloaded Projects");
                try
                {
                    using (var client = new WebClient())
                    {
                        progressBar1.Visible = true;
                        label5.Visible = true;
                        client.DownloadProgressChanged += (s, v) =>
                        {
                            progressBar1.Value = v.ProgressPercentage;
                        };
                        client.DownloadFileCompleted += (s, v) =>
                        {
                            progressBar1.Visible = false;
                            label5.Visible = false;
                            DialogResult dialog = MessageBox.Show(strings.DownloadSuccessful, "My GBA Library", MessageBoxButtons.YesNo);
                            if (dialog == DialogResult.Yes)
                            {
                                if(!(curFile.Substring(curFile.LastIndexOf('.')).Equals(".zip") || curFile.Substring(curFile.LastIndexOf('.')).Equals(".ups")))
                                {
                                    MessageBox.Show(strings.NoUPSFilesFound, "My GBA Library");
                                }
                                else
                                {
                                    PatchForm form;
                                    if (curFile.Substring(curFile.LastIndexOf('.')).Equals(".zip"))
                                    {
                                        ZipFile.ExtractToDirectory("Downloaded Projects/" + curFile, "Downloaded Projects/" + curFile.Substring(0, curFile.LastIndexOf('.')));
                                        form = new PatchForm(mainform, true, "Downloaded Projects/" + curFile.Substring(0, curFile.LastIndexOf('.')));
                                    }
                                    else
                                    {
                                        form = new PatchForm(mainform, false, "Downloaded Projects/" + curFile);
                                    }
                                    form.ShowDialog();
                                }
                            }
                        };
                        client.DownloadFileAsync(new Uri(curDllink), "Downloaded Projects\\" + curFile);
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                }
            }
        }
    }
}

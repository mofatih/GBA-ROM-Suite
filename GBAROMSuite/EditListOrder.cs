﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using CsvHelper;
using MyGBALibrary.Properties;

namespace MyGBALibrary
{
    public partial class EditListOrder : Form
    {
        string baseROM;
        bool unsavedChanges = false;
        public EditListOrder(string baseROM)
        {
            InitializeComponent();
            this.baseROM = baseROM;
            if (baseROM == null)
            {
                using (var reader = new StreamReader("Games/rom_mapping.csv"))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    while (csv.Read())
                    {
                        string value;
                        csv.TryGetField<string>(0, out value);
                        listBox2.Items.Add(value);
                    }
                }
            }
            else
            {
                using (var reader = new StreamReader("Games/rom_mapping.csv"))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    while (csv.Read())
                    {
                        string value;
                        csv.TryGetField<string>(0, out value);
                        if (value.Equals(baseROM))
                        {
                            for (int i = 1; csv.TryGetField<string>(i, out value); i++)
                            {
                                listBox2.Items.Add(value);
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void saveChanges()
        {
            List<List<string>> newFileContents = new List<List<string>>();
            if (baseROM == null)
            {
                foreach (object o in listBox2.Items)
                {
                    using (var reader = new StreamReader("Games/rom_mapping.csv"))
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.HasHeaderRecord = false;
                        while (csv.Read())
                        {
                            string value;
                            csv.TryGetField<string>(0, out value);
                            if (o.ToString().Equals(value))
                            {
                                List<string> newRow = new List<string>();
                                for (int i = 0; csv.TryGetField<string>(i, out value); i++)
                                {
                                    newRow.Add(value);
                                }
                                newFileContents.Add(newRow);
                                break;
                            }
                        }
                    }
                }
                using (var writer = new StreamWriter("Games/rom_mapping.csv"))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    foreach (List<string> l in newFileContents)
                    {
                        foreach (string s in l)
                        {
                            csv.WriteField(s);
                        }
                        csv.NextRecord();
                    }
                }
            }
            else
            {
                using (var reader = new StreamReader("Games/rom_mapping.csv"))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    while (csv.Read())
                    {
                        string value;
                        List<string> newRow = new List<string>();
                        csv.TryGetField<string>(0, out value);
                        if (baseROM.Equals(value))
                        {
                            newRow.Add(baseROM);
                            foreach(string item in listBox2.Items)
                            {
                                newRow.Add(item);
                            }
                        }
                        else
                        {
                            for (int i = 0; csv.TryGetField<string>(i, out value); i++)
                            {
                                newRow.Add(value);
                            }
                        }
                        newFileContents.Add(newRow);
                    }
                }
                using (var writer = new StreamWriter("Games/rom_mapping.csv"))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    foreach (List<string> l in newFileContents)
                    {
                        foreach (string s in l)
                        {
                            csv.WriteField(s);
                        }
                        csv.NextRecord();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                int index = listBox2.SelectedIndex;
                if(index > 0)
                {
                    object item = listBox2.SelectedItem;
                    listBox2.Items.RemoveAt(index);
                    listBox2.Items.Insert(index - 1, item);
                    unsavedChanges = true;
                    listBox2.SelectedIndex = index - 1;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                int index = listBox2.SelectedIndex;
                if (index < listBox2.Items.Count - 1)
                {
                    object item = listBox2.SelectedItem;
                    listBox2.Items.RemoveAt(index);
                    listBox2.Items.Insert(index + 1, item);
                    unsavedChanges = true;
                    listBox2.SelectedIndex = index + 1;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            saveChanges();
            unsavedChanges = false;
            MessageBox.Show(strings.ChangesSaved, "My GBA Library");
        }

        private void EditListOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (unsavedChanges)
            {
                DialogResult dialogResult = MessageBox.Show(strings.SaveChanges, "My GBA Library", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    saveChanges();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

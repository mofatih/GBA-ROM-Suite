﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic;
using CsvHelper;
using System.Globalization;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Threading;
using MyGBALibrary.Properties;

namespace MyGBALibrary
{
    public partial class Main : Form
    {
        List<int> ids = new List<int>();
        List<string> names = new List<string>();
        Dictionary<string, List<string>> mapping = new Dictionary<string, List<string>>();
        public string multiPatchErrorString;
        public int successfulPatches;
        public Main()
        {
            Directory.CreateDirectory("Games");
            Console.WriteLine(Settings.Default["firstTime"].ToString());
            if ((bool)(Settings.Default["firstTime"]) == true)
            {
                SetupForm form = new SetupForm();
                form.ShowDialog();
            }
            switch (Settings.Default["Language"])
            {
                case "English":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                    break;
                case "简体中文":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("zh");
                    break;
                case "日本語":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ja");
                    break;
            }
            InitializeComponent();
            if (!File.Exists("Games/rom_mapping.csv"))
            {
                File.Create("Games/rom_mapping.csv").Close();
            }
            else
            {
                string line;
                System.IO.StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                while ((line = file.ReadLine()) != null)
                {
                    var arr = line.Split(',');
                    listBox1.Items.Add(arr[0]);
                    var arr2 = arr.ToList().GetRange(1, arr.Length - 1);
                    mapping.Add(arr[0], arr2);
                }
                file.Close();
            }
            comboBox1.SelectedIndex = 0;
            initFEC();
        }

        //HELPER FUNCTIONS

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public System.Windows.Forms.ListBox getROMList()
        {
            return listBox1;
        }

        public string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        private void initROMs(bool fromAddROM)
        {
            listBox1.Items.Clear();
            string line;
            System.IO.StreamReader file =
                new System.IO.StreamReader(@"Games/rom_mapping.csv");
            while ((line = file.ReadLine()) != null)
            {
                var arr = line.Split(',');
                if (placeHolderTextBox2.Text.Length == 0 || (!placeHolderTextBox2.Focused && !fromAddROM) || arr[0].ToLower().Contains(placeHolderTextBox2.Text.ToLower()))
                {
                    listBox1.Items.Add(arr[0]);
                }
            }
            file.Close();
            initHacks(false);
        }

        public void initHacks(bool fromAddPatch)
        {
            listBox2.Items.Clear();
            if (listBox1.SelectedItem != null)
            {
                using (var reader = new StreamReader("Games/rom_mapping.csv"))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    while (csv.Read())
                    {
                        string value;
                        csv.TryGetField<string>(0, out value);
                        if (listBox1.SelectedItem.ToString().Equals(value))
                        {
                            for (int i = 1; csv.TryGetField<string>(i, out value); i++)
                            {
                                if (placeHolderTextBox3.Text.Length == 0 || (!placeHolderTextBox3.Focused && !fromAddPatch) || value.ToLower().Contains(placeHolderTextBox3.Text.ToLower()))
                                {
                                    listBox2.Items.Add(value);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        public void applyPatch(string baseROM, string patch)
        {
            string patchName = Path.GetFileName(patch);
            string hackName = patchName.Substring(0, patchName.IndexOf(".ups")) + ".gba";
            Directory.CreateDirectory("Games/Patched ROMs");
            if (!File.Exists("Games/Patched ROMs/" + hackName))
            {
                try
                {
                    byte[] patchBytes = File.ReadAllBytes(patch);
                    byte[] origROMBytes = File.ReadAllBytes("Games/Base ROMs/" + baseROM);
                    var newROMBytes = origROMBytes.ToList();

                    int filePtr = 0;
                    long index = 12;
                    while (index < patchBytes.Length - 12)
                    {
                        int ptrDiff = 0, shift = 1;
                        while (true)
                        {
                            byte x = patchBytes[index++];
                            ptrDiff += (x & 0x7f) * shift;
                            if ((x & 0x80) != 0) break;
                            shift <<= 7;
                            ptrDiff += shift;
                        }
                        filePtr += ptrDiff;
                        if (filePtr > newROMBytes.Count() - 1)
                        {
                            newROMBytes.AddRange(Enumerable.Repeat((byte)0, filePtr - newROMBytes.Count()));
                        }
                        while (patchBytes[index] != 0)
                        {
                            if (filePtr > newROMBytes.Count() - 1)
                            {
                                newROMBytes.Add(patchBytes[index++]);
                                filePtr++;
                            }
                            else
                            {
                                newROMBytes[filePtr] = (byte)(origROMBytes[filePtr++] ^ patchBytes[index++]);
                            }
                        }
                        index++;
                        filePtr++;
                    }
                    string line;
                    List<string> linesToWrite = new List<string>();
                    StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                    while ((line = file.ReadLine()) != null)
                    {
                        if (line.Split(',')[0] == baseROM)
                        {
                            string restOfLine = "";
                            if (line.IndexOf(',') != -1) restOfLine = line.Substring(line.IndexOf(','));
                            linesToWrite.Add(line.Split(',')[0] + "," + hackName + restOfLine);
                        }
                        else
                        {
                            linesToWrite.Add(line);
                        }
                    }
                    file.Close();
                    File.WriteAllText("Games/rom_mapping.csv", string.Empty);
                    using (StreamWriter sw = File.AppendText("Games/rom_mapping.csv"))
                    {
                        foreach (string i in linesToWrite)
                        {
                            sw.WriteLine(i);
                        }
                        mapping[baseROM].Add(hackName);
                    }
                    File.WriteAllBytes("Games/Patched ROMs/" + hackName, newROMBytes.ToArray());

                    listBox2.Items.Insert(0,hackName);
                    successfulPatches++;
                }
                catch (Exception ex)
                {
                    multiPatchErrorString += hackName + strings.PatchingError + "\n";
                }
            }
            else
            {
                multiPatchErrorString += hackName + strings.HackAlreadyExists + "\n";
            }
        }

        private void openROM(string path)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Settings.Default["EmulatorPath"].ToString();
                startInfo.Arguments = "\"" + path + "\"";
                Process.Start(startInfo);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show(strings.SetEmulator, "My GBA Library");
            }
        }

        private void initFEC()
        {
            ids.Clear();
            names.Clear();
            listView1.Clear();
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    var json = System.Net.WebUtility.HtmlDecode(wc.DownloadString("https://fecentral.org/api/getHack.php"));
                    JArray jarray = JArray.Parse(json);
                    switch (comboBox1.SelectedIndex)
                    {
                        case 0:
                            var ver = System.Net.WebUtility.HtmlDecode(wc.DownloadString("https://fecentral.org/api/getVersions.php"));
                            JArray versions = JArray.Parse(ver);
                            var grouped = versions.GroupBy(i => i["hackid"]).Select(g => new
                            {
                                hackid = g.Key,
                                date = g.Max(row => row.Value<int>("date"))
                            }).OrderBy(i => i.date).Reverse().Join(jarray, version => version.hackid, hack => hack["id"], (version, hack) => new
                            {
                                version.hackid,
                                name = hack["name"],
                                engine = hack["engine"],
                            });
                            foreach (var row in grouped)
                            {
                                if ((row.engine.ToString() == "FE7" && checkBox1.Checked) || (row.engine.ToString() == "FE8" && checkBox2.Checked) || (row.engine.ToString() != "FE8" && row.engine.ToString() != "FE7" && checkBox3.Checked))
                                {
                                    if (placeHolderTextBox1.Text.Length == 0 || !placeHolderTextBox1.Focused)
                                    {
                                        listView1.Items.Add(row.name.ToString());
                                        ids.Add(row.hackid.ToObject<int>());
                                        names.Add(row.name.ToString());
                                    }
                                    else if (row.name.ToString().ToLower().Contains(placeHolderTextBox1.Text.ToLower()))
                                    {
                                        listView1.Items.Add(row.name.ToString());
                                        ids.Add(row.hackid.ToObject<int>());
                                        names.Add(row.name.ToString());
                                    }
                                }
                            }
                            break;
                        case 1:
                            var grouped2 = jarray.OrderByDescending(i => i.Value<int>("viewcount"));
                            foreach (var row in grouped2)
                            {
                                if ((row.Value<string>("engine") == "FE7" && checkBox1.Checked) || (row.Value<string>("engine") == "FE8" && checkBox2.Checked) || (row.Value<string>("engine") != "FE8" && row.Value<string>("engine") != "FE7" && checkBox3.Checked))
                                {
                                    if (placeHolderTextBox1.Text.Length == 0 || !placeHolderTextBox1.Focused)
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                    else if (row.Value<string>("name").ToLower().Contains(placeHolderTextBox1.Text.ToLower()))
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                }
                            }
                            break;
                        case 2:
                            var grouped3 = jarray.OrderByDescending(i => i.Value<int>("downloads"));
                            foreach (var row in grouped3)
                            {
                                if ((row.Value<string>("engine") == "FE7" && checkBox1.Checked) || (row.Value<string>("engine") == "FE8" && checkBox2.Checked) || (row.Value<string>("engine") != "FE8" && row.Value<string>("engine") != "FE7" && checkBox3.Checked))
                                {
                                    if (placeHolderTextBox1.Text.Length == 0 || !placeHolderTextBox1.Focused)
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                    else if (row.Value<string>("name").ToLower().Contains(placeHolderTextBox1.Text.ToLower()))
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                }
                            }
                            break;
                        case 3:
                            var grouped4 = jarray.OrderBy(i => i["name"]);
                            foreach (var row in grouped4)
                            {
                                if ((row.Value<string>("engine") == "FE7" && checkBox1.Checked) || (row.Value<string>("engine") == "FE8" && checkBox2.Checked) || (row.Value<string>("engine") != "FE8" && row.Value<string>("engine") != "FE7" && checkBox3.Checked))
                                {
                                    if (placeHolderTextBox1.Text.Length == 0 || !placeHolderTextBox1.Focused)
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                    else if (row.Value<string>("name").ToLower().Contains(placeHolderTextBox1.Text.ToLower()))
                                    {
                                        listView1.Items.Add(row.Value<string>("name"));
                                        ids.Add(row.Value<int>("id"));
                                        names.Add(row.Value<string>("name"));
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        //CONTROL EVENTS

        private void button8_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        string[] files = Directory.GetFiles(dialog.SelectedPath, "*.ups", SearchOption.AllDirectories);
                        if (MessageBox.Show(files.Length + strings.MultiPatchAreYouSure, "My GBA Library", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            multiPatchErrorString = "";
                            successfulPatches = 0;
                            foreach (String file in files)
                            {
                                applyPatch(listBox1.SelectedItem.ToString(), file);
                            }
                            multiPatchErrorString = successfulPatches + strings.MultiPatchSuccess + "\n\n" + multiPatchErrorString;
                            MessageBox.Show(multiPatchErrorString, "My GBA Library");
                            initHacks(true);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(strings.SelectROM, "My GBA Library");
            }
        }

        private void addPatchButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                OpenFileDialog fdlg = new OpenFileDialog();
                fdlg.Title = "My GBA Library";
                fdlg.InitialDirectory = @"./";
                fdlg.Filter = "UPS files (*.ups)|*.ups";
                fdlg.Multiselect = true;
                fdlg.FilterIndex = 1;
                fdlg.RestoreDirectory = true;
                if (fdlg.ShowDialog() == DialogResult.OK)
                {
                    multiPatchErrorString = "";
                    successfulPatches = 0;
                    if (MessageBox.Show(fdlg.FileNames.Length + strings.MultiPatchAreYouSure, "My GBA Library", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (String file in fdlg.FileNames)
                        {
                            applyPatch(listBox1.SelectedItem.ToString(), file);
                        }
                        multiPatchErrorString = successfulPatches + strings.MultiPatchSuccess + "\n\n" + multiPatchErrorString;
                        MessageBox.Show(multiPatchErrorString, "My GBA Library");
                        initHacks(true);
                    }
                }
            }
            else
            {
                MessageBox.Show(strings.SelectROM,"My GBA Library");
            }
        }

        private void addROMButton_MouseClick(object sender, MouseEventArgs e)
        {
            string[] arr;
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "My GBA Library";
            fdlg.InitialDirectory = @"./";
            fdlg.Filter = "GBA files (*.gba)|*.gba";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                arr = fdlg.FileName.Split('\\');
                Directory.CreateDirectory("Games/Base ROMs");
                try
                {
                    File.Copy(fdlg.FileName, "Games/Base ROMs/" + arr[arr.Length - 1]);
                    string currentContent = File.ReadAllText("Games/rom_mapping.csv");
                    File.WriteAllText("Games/rom_mapping.csv", arr[arr.Length - 1] + "\n" + currentContent);
                    mapping.Add(arr[arr.Length - 1], new List<string> { });
                    listBox1.Items.Insert(0, arr[arr.Length - 1]);
                }
                catch(Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "My GBA Library");
                    File.Delete("Games/Base ROMs/" + arr[arr.Length - 1]);
                }
            }
            initROMs(true);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            initHacks(true);
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                if (!File.Exists("Games/Base ROMs/" + listBox1.SelectedItem.ToString()))
                {
                    System.Windows.Forms.MessageBox.Show(strings.ROMNotFound, "My GBA Library");
                }
                else
                {
                    openROM("Games/Base ROMs/" + listBox1.SelectedItem.ToString());
                }
            }
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox2.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                if (!File.Exists("Games/Patched ROMs/" + listBox2.SelectedItem.ToString()))
                {
                    System.Windows.Forms.MessageBox.Show(strings.ROMNotFound, "My GBA Library");
                }
                else
                {
                    openROM("Games/Patched ROMs/" + listBox2.SelectedItem.ToString());
                }
            }
        }

        private void listBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = this.listBox1.IndexFromPoint(e.Location);
                if (index != System.Windows.Forms.ListBox.NoMatches)
                {
                    listBox1.SelectedIndex = index;
                    contextMenuStrip1.Show(MousePosition);
                }
            }
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "playToolStripMenuItem":
                    contextMenuStrip1.Close();
                    if (!File.Exists("Games/Base ROMs/" + listBox1.SelectedItem.ToString()))
                    {
                        System.Windows.Forms.MessageBox.Show(strings.ROMNotFound, "My GBA Library");
                    }
                    else
                    {
                        openROM("Games/Base ROMs/" + listBox1.SelectedItem.ToString());
                    }
                    break;
                case "deleteToolStripMenuItem":
                    contextMenuStrip1.Close();
                    DialogResult dialogResult = System.Windows.Forms.MessageBox.Show(strings.DeleteROM, "My GBA Library", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (mapping[listBox1.SelectedItem.ToString()].Count > 0)
                        {
                            foreach (string s in mapping[listBox1.SelectedItem.ToString()])
                            {
                                File.Delete("Games/Patched ROMs/" + s);
                                File.Delete("Games/Patched ROMs/" + s.Substring(0, s.IndexOf('.')) + ".sav");
                            }
                        }
                        File.Delete("Games/Base ROMs/" + listBox1.SelectedItem.ToString());
                        File.Delete("Games/Base ROMs/" + listBox1.SelectedItem.ToString().Substring(0, listBox1.SelectedItem.ToString().IndexOf('.')) + ".sav");
                        mapping.Remove(listBox1.SelectedItem.ToString());
                        string line;
                        List<string> readLines = new List<string>();
                        StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.Split(',')[0] != listBox1.SelectedItem.ToString())
                            {
                                readLines.Add(line);
                            }
                        }
                        file.Close();
                        File.WriteAllText("Games/rom_mapping.csv", string.Empty);
                        using (StreamWriter sw = File.AppendText("Games/rom_mapping.csv"))
                        {
                            foreach (string i in readLines)
                            {
                                sw.WriteLine(i);
                            }
                        }
                        listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                        listBox2.Items.Clear();
                    }
                    break;
                case "renameToolStripMenuItem":
                    contextMenuStrip1.Close();
                    string oldName = listBox1.SelectedItem.ToString();
                    string newName = Interaction.InputBox(strings.NewName, "My GBA Library", oldName.Substring(0, listBox1.SelectedItem.ToString().LastIndexOf('.')));
                    newName = newName + ".gba";
                    if (!newName.Equals(listBox1.SelectedItem.ToString()))
                    {
                        mapping.Add(newName, mapping[oldName]);
                        mapping.Remove(oldName);
                        File.Move("Games/Base ROMs/" + oldName, "Games/Base ROMs/" + newName);
                        if (File.Exists("Games/Base ROMs/" + oldName.Substring(0, oldName.LastIndexOf('.')) + ".sav"))
                        {
                            File.Move("Games/Base ROMs/" + oldName.Substring(0, oldName.LastIndexOf('.')) + ".sav", "Games/Base ROMs/" + newName.Substring(0, newName.LastIndexOf('.')) + ".sav");
                        }
                        listBox1.Items[listBox1.SelectedIndex] = newName;
                        string line;
                        List<string> linesToWrite = new List<string>();
                        StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.StartsWith(oldName))
                            {
                                linesToWrite.Add(ReplaceFirst(line, oldName, newName));
                            }
                            else
                            {
                                linesToWrite.Add(line);
                            }
                        }
                        file.Close();
                        File.WriteAllText("Games/rom_mapping.csv", string.Empty);
                        using (StreamWriter sw = File.AppendText("Games/rom_mapping.csv"))
                        {
                            foreach (string i in linesToWrite)
                            {
                                sw.WriteLine(i);
                            }
                        }
                        initHacks(false);
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(strings.RenameError, "My GBA Library");
                    }
                    break;
                case "importsavFileToolStripMenuItem":
                    contextMenuStrip1.Close();
                    string name = listBox1.SelectedItem.ToString().Substring(0, listBox1.SelectedItem.ToString().LastIndexOf('.')) + ".sav";
                    if (File.Exists("Games/Base ROMs/" + name))
                    {
                        if (MessageBox.Show(strings.ImportSave, "My GBA Library", MessageBoxButtons.YesNo) == DialogResult.No)
                        {
                            break;
                        }
                    }
                    OpenFileDialog fdlg = new OpenFileDialog();
                    fdlg.Title = "My GBA Library";
                    fdlg.InitialDirectory = @"./";
                    fdlg.Filter = "SAV files (*.sav)|*.sav";
                    fdlg.FilterIndex = 1;
                    fdlg.RestoreDirectory = true;
                    if (fdlg.ShowDialog() == DialogResult.OK)
                    {
                        File.Delete("Games/Base ROMs/" + name);
                        File.Copy(fdlg.FileName, "Games/Base ROMs/" + name);
                    }
                    break;
            }
        }

        private void listBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = this.listBox2.IndexFromPoint(e.Location);
                if (index != System.Windows.Forms.ListBox.NoMatches)
                {
                    listBox2.SelectedIndex = index;
                    contextMenuStrip2.Show(MousePosition);
                }
            }
        }

        private void contextMenuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "playToolStripMenuItem1":
                    contextMenuStrip2.Close();
                    if (!File.Exists("Games/Patched ROMs/" + listBox2.SelectedItem.ToString()))
                    {
                        System.Windows.Forms.MessageBox.Show(strings.ROMNotFound, "My GBA Library");
                    }
                    else
                    {
                        openROM("Games/Patched ROMs/" + listBox2.SelectedItem.ToString());
                    }
                    break;
                case "deleteToolStripMenuItem1":
                    contextMenuStrip2.Close();
                    DialogResult dialogResult = System.Windows.Forms.MessageBox.Show(strings.DeleteHack, "My GBA Library", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string name = listBox2.SelectedItem.ToString();
                        File.Delete("Games/Patched ROMs/" + name);
                        File.Delete("Games/Patched ROMs/" + name.Substring(0, name.IndexOf('.')) + ".sav");
                        mapping[listBox1.SelectedItem.ToString()].Remove(listBox2.SelectedItem.ToString());
                        string line;
                        List<string> readLines = new List<string>();
                        StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.Split(',')[0] != listBox1.SelectedItem.ToString())
                            {
                                readLines.Add(line);
                            }
                            else
                            {
                                readLines.Add(line.Replace("," + name, ""));
                            }
                        }
                        file.Close();
                        File.WriteAllText("Games/rom_mapping.csv", string.Empty);
                        using (StreamWriter sw = File.AppendText("Games/rom_mapping.csv"))
                        {
                            foreach (string i in readLines)
                            {
                                sw.WriteLine(i);
                            }
                        }
                        listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                    }
                    break;
                case "renameToolStripMenuItem1":
                    contextMenuStrip2.Close();
                    string oldName = listBox2.SelectedItem.ToString();
                    string newName = Interaction.InputBox(strings.NewName, "My GBA Library", oldName.Substring(0, oldName.LastIndexOf('.')));
                    newName = newName + ".gba";
                    if (!newName.Equals(listBox2.SelectedItem.ToString()))
                    {
                        mapping[listBox1.SelectedItem.ToString()].Add(newName);
                        mapping[listBox1.SelectedItem.ToString()].Remove(oldName);
                        File.Move("Games/Patched ROMs/" + oldName, "Games/Patched ROMs/" + newName);
                        if (File.Exists("Games/Patched ROMs/" + oldName.Substring(0, oldName.LastIndexOf('.')) + ".sav"))
                        {
                            File.Move("Games/Patched ROMs/" + oldName.Substring(0, oldName.LastIndexOf('.')) + ".sav", "Games/Patched ROMs/" + newName.Substring(0, newName.IndexOf('.')) + ".sav");
                        }
                        listBox2.Items[listBox2.SelectedIndex] = newName;
                        string line;
                        List<string> linesToWrite = new List<string>();
                        StreamReader file = new System.IO.StreamReader(@"Games/rom_mapping.csv");
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.StartsWith(listBox1.SelectedItem.ToString()))
                            {
                                linesToWrite.Add(ReplaceFirst(line,oldName, newName));
                            }
                            else
                            {
                                linesToWrite.Add(line);
                            }
                        }
                        file.Close();
                        File.WriteAllText("Games/rom_mapping.csv", string.Empty);
                        using (StreamWriter sw = File.AppendText("Games/rom_mapping.csv"))
                        {
                            foreach (string i in linesToWrite)
                            {
                                sw.WriteLine(i);
                            }
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(strings.RenameError, "My GBA Library");
                    }
                    break;
                case "importsavFileToolStripMenuItem1":
                    contextMenuStrip2.Close();
                    string name2 = listBox2.SelectedItem.ToString().Substring(0, listBox2.SelectedItem.ToString().LastIndexOf('.')) + ".sav";
                    if (File.Exists("Games/Patched ROMs/" + name2))
                    {
                        if (MessageBox.Show(strings.ImportSave, "My GBA Library", MessageBoxButtons.YesNo) == DialogResult.No)
                        {
                            break;
                        }
                    }
                    OpenFileDialog fdlg = new OpenFileDialog();
                    fdlg.Title = "My GBA Library";
                    fdlg.InitialDirectory = @"./";
                    fdlg.Filter = "SAV files (*.sav)|*.sav";
                    fdlg.FilterIndex = 1;
                    fdlg.RestoreDirectory = true;
                    if (fdlg.ShowDialog() == DialogResult.OK)
                    {
                        File.Delete("Games/Patched ROMs/" + name2);
                        File.Copy(fdlg.FileName, "Games/Patched ROMs/" + name2);
                    }
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            initFEC();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            int hackid = ids[listView1.SelectedIndices[0]];
            DownloadForm form = new DownloadForm(hackid, this);
            form.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            initFEC();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            initFEC();
        }

        private void placeHolderTextBox1_TextChanged(object sender, EventArgs e)
        {
            if(!placeHolderTextBox1.isPlaceHolder) initFEC();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            initFEC();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            initFEC();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                string patchName;
                OpenFileDialog fdlg = new OpenFileDialog();
                fdlg.Title = "My GBA Library";
                fdlg.InitialDirectory = @"./";
                fdlg.Filter = "GBA files (*.gba)|*.gba";
                fdlg.FilterIndex = 1;
                fdlg.RestoreDirectory = true;
                if (fdlg.ShowDialog() == DialogResult.OK)
                {
                    var arr = fdlg.FileName.Split('\\');
                    patchName = arr[arr.Length - 1].Substring(0, arr[arr.Length - 1].ToLower().IndexOf(".gba")) + ".ups";
                    Directory.CreateDirectory("New Patches");
                    try
                    {
                        byte[] newROMBytes = File.ReadAllBytes(fdlg.FileName);
                        byte[] origROMBytes = File.ReadAllBytes("Games/Base ROMs/" + listBox1.SelectedItem.ToString());
                        List<byte> patchBytes = new List<byte>();

                        patchBytes.Add((byte)0x55); patchBytes.Add((byte)0x50); patchBytes.Add((byte)0x53); patchBytes.Add((byte)0x31);
                        FileInfo file = new FileInfo("Games/Base ROMs/" + listBox1.SelectedItem.ToString());
                        int size = (int)file.Length;
                        patchBytes.Add((byte)size); patchBytes.Add((byte)(size >> 8)); patchBytes.Add((byte)(size >> 16)); patchBytes.Add((byte)(size >> 24));
                        file = new FileInfo(fdlg.FileName);
                        size = (int)file.Length;
                        patchBytes.Add((byte)size); patchBytes.Add((byte)(size >> 8)); patchBytes.Add((byte)(size >> 16)); patchBytes.Add((byte)(size >> 24));
                        
                        long index = 0, data = -1;
                        while (index < newROMBytes.Length)
                        {
                            while(index < origROMBytes.Length && newROMBytes[index] == origROMBytes[index])
                            {
                                index++;
                            }
                            while (index > origROMBytes.Length - 1 && index < newROMBytes.Length && newROMBytes[index] == 0)
                            {
                                index++;
                            }
                            if (index > newROMBytes.Length - 1) break;
                            data = index - 1 - data;
                            while (true)
                            {
                                byte x = (byte)(data & 0x7f);
                                data >>= 7;
                                if (data == 0)
                                {
                                    patchBytes.Add((byte)(0x80 | x));
                                    break;
                                }
                                patchBytes.Add(x);
                                data--;
                            }
                            while(index < newROMBytes.Length)
                            {
                                if (index > origROMBytes.Length - 1 && newROMBytes[index] != 0) patchBytes.Add(newROMBytes[index]);
                                else if (index < origROMBytes.Length && newROMBytes[index] != origROMBytes[index]) patchBytes.Add((byte)(newROMBytes[index] ^ origROMBytes[index]));
                                else break;
                                index++;
                            }
                            data = index;
                            patchBytes.Add(0);
                        }
                        for (int i = 0; i < 12; i++) patchBytes.Add(0);
                        File.WriteAllBytes("New Patches/" + patchName, patchBytes.ToArray());
                        System.Windows.Forms.MessageBox.Show(strings.PatchCreated, "My GBA Library");
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "My GBA Library");
                    }
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show(strings.CreatePatchSelectROM, "My GBA Library");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.fecentral.org/");
        }

        private void placeHolderTextBox2_TextChanged(object sender, EventArgs e)
        {
            initROMs(false);
        }

        private void placeHolderTextBox3_TextChanged(object sender, EventArgs e)
        {
            initHacks(false);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            EditListOrder form = new EditListOrder(null);
            form.ShowDialog();
            initROMs(false);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedItems.Count > 0)
            {
                EditListOrder form = new EditListOrder(listBox1.SelectedItem.ToString());
                form.ShowDialog();
                initHacks(false);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SetupForm form = new SetupForm();
            var result = form.ShowDialog();
            if(result == DialogResult.Yes)
            {
                this.Close();
                Application.Restart();
                Environment.Exit(0);
            }
        }
    }
}

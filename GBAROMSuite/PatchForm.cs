﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using MyGBALibrary.Properties;

namespace MyGBALibrary
{
    public partial class PatchForm : Form
    {
        Main mainform;
        bool multiPatch;
        string path;
        public PatchForm( Main mainform, bool multiPatch, string path )
        {
            InitializeComponent();
            this.mainform = mainform;
            this.multiPatch = multiPatch;
            this.path = path;
            listBox1.Items.AddRange(mainform.getROMList().Items);
        }

        private void select()
        {
            if (listBox1.SelectedItem != null)
            {
                List<string> files = new List<string>();
                DialogResult result = DialogResult.Yes;
                if (this.multiPatch)
                {
                    files.AddRange(Directory.GetFiles(path, "*.ups", SearchOption.AllDirectories));
                    result = MessageBox.Show(files.Count + strings.MultiPatchAreYouSure, "My GBA Library", MessageBoxButtons.YesNo);
                }
                else files.Add(path);
                Console.WriteLine(files[0]);
                if (result == DialogResult.Yes)
                {
                    mainform.multiPatchErrorString = "";
                    mainform.successfulPatches = 0;
                    foreach (String file in files)
                    {
                        mainform.applyPatch(listBox1.SelectedItem.ToString(), file);
                    }
                    mainform.multiPatchErrorString = mainform.successfulPatches + strings.MultiPatchSuccess + "\n\n" + mainform.multiPatchErrorString;
                    MessageBox.Show(mainform.multiPatchErrorString, "My GBA Library");
                    mainform.initHacks(true);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show(strings.SelectROM, "My GBA Library");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                select();
            }
        }
    }
}

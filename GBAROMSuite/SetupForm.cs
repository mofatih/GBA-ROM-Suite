﻿using MyGBALibrary.Properties;
using System;
using System.Windows.Forms;

namespace MyGBALibrary
{
    public partial class SetupForm : Form
    {
        int lang;
        public SetupForm()
        {
            InitializeComponent();
            this.DialogResult = DialogResult.No;
            switch (Settings.Default["Language"].ToString())
            {
                case "English":
                    this.lang = 0;
                    break;
                case "简体中文":
                    this.lang = 1;
                    break;
                case "日本語":
                    this.lang = 2;
                    break;
            }
            if((bool)Settings.Default["firstTime"]) comboBox1.SelectedIndex = 0;
            else comboBox1.SelectedIndex = lang;
            textBox1.Text = Settings.Default["EmulatorPath"].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Settings.Default["EmulatorPath"] = textBox1.Text;
            Settings.Default["Language"] = comboBox1.SelectedItem.ToString();
            Settings.Default["firstTime"] = false;
            Settings.Default.Save();
            if (comboBox1.SelectedIndex != lang) this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "My GBA Library";
            fdlg.InitialDirectory = @"./";
            fdlg.Filter = "All files (*)|*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fdlg.FileName;
            }
        }
    }
}

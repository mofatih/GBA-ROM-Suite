My GBA Library v2.1

---------------

ADDING ROMS AND APPLYING PATCHES

Click the "Add ROM" button to add a new base GBA ROM (note: not limited to FE ROMs). It will appear in the left-most list.

To add a new hack, first select the base ROM to apply the patch to, then click "Apply Patch." Select the ups patch you would like to apply. Then, if the patching was successful, the new hack will appear in the list under the Apply Patch button.

You can also use the Apply Patches by Folder button to patch all ups files in a certain folder to the selected base ROM and add them to your library.

---------------

PLAYING ROMS

Double-click on any ROM or hack to play it. You must set your emulator via the button in the bottom-left corner before you can play.

IMPORTANT: You can view your .gba and .sav files in the "Games" folder but if you remove or rename them My GBA Library may not function properly.

---------------

CREATING PATCHES

Select a base ROM from the list of base ROMs you added. Click "Create Patch," then select your modified ROM. If successful, your new patch should appear in a folder called "New Patches."

IMPORTANT: I cannot guarantee that patches created through this application will work correctly on other software. They will most likely work when applied through My GBA Library, however.

---------------

DOWNLOADING HACKS

Use the filter options in the top right to access a list of uploaded hacks on FECentral.org. Double-click a hack name to preview it, after which you can download the hack or go to the FECentral page for it. You will then be given the option to immediately patch it to a base ROM of your choice. All downloaded files will appear in the Downloaded Projects folder of the same directory as the application.

---------------

Chinese translation by wzdret
Japanese translation by 7743

Credit to Mystletainn for the icon and Fatih for the error image
Thanks to Markyjoe for creating and giving me access to FECentral
Thanks to 7743 for teaching me about web requests
Thanks to byuu for making UPS patching possible

Enjoy! -Pwntagonist
thepwntagonist@gmail.com